# -*- coding: UTF-8 -*-
# Copyright 2025 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from .mixins import XMLMaker
from .choicelists import JinjaBuildMethod
