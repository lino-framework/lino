# Copyright 2014-2018 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""

Documentation is being migrated to prosa style in :doc:`/api/index`.

Another set of modules defined here are for more technical usage in
specialized context:

.. autosummary::
   :toctree:

   doctest
   shell
   selenium
"""

from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy as pgettext
from django.utils.translation import gettext
